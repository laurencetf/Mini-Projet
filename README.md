README mini projet application des tris 


Notre programme répond aux deux problèmes posés.

Installation :

Commande à saisir dans le terminal pour la compilation : fpc Miniprojet.pas et pour l'exécution  : ./Miniprojet .


Dans le menu principal, il faut choisir entre Réincarnation ou Tournoi de football ( le programme renvoie ce menu pricipal tant que  l'utilisateur n'a pas saisie '0').

Réincarnation  :
L'utilisateur doit saisir le nombre de prénom qu'il souhaite comparer, puis écrire ses prénoms.
(écrire en minuscule ou en majuscule, cependant il faut enlever tous les accents).
Le programme, après avoir calculer le nombre de réincarnation de chaque prénom, trie ses prénoms dans l'ordre croissant. 

Tournoi de football :
Pour commencer, l'utilisateur doit saisir le nombre d'équipe, le nom de ses équipes, ainsi que les résultats entre ces équipes. 
Pour la saisie des résultats,  il faut suivre le modèle suivant :
 exemple : Arsenal/MAN U : 3 0 
Le programme renvoie le classement du tournoi.


Programme réalisé par BRUGAT Paul, LAURENCET Fabien et LESCAUD Maxime.

