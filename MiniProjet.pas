
(*
 -------------------------------------------------------------------
 Nom Programme    :  miniprojet.pas
 Auteur           :  Brugat Paul <brugatpaul@eisti.eu>
 Description      :  <description>
 Date de création :  Tue Mar  1 10:34:09 2016
 Compilation      :  fpc
 Execution        :  shell
 -------------------------------------------------------------------
 *)

PROGRAM  miniprojet;
USES crt,sysutils;

type
    {Structure de stockage des prénoms }
   data       = record
                   name : string;
                   nb   : integer;
                end;    
   tabdata    = array of data;
   
   {Structure de stockage de la diiférence entre les buts marqués et encaissés }
   difference = record
                   points : integer;
                   score  : string;
                end;

{ Structure de stockage des matchs }
   match     = record
		    equipe1 : string;
		    nbBut1  : integer;
		    equipe2 : string;
		    nbBut2  : integer;
		 end;	    
   tabmatch = array of match;

{Structure de stockage des paramètres en cas d'égalité }
   egal=record
           ideb   : integer;
           ifin   : integer;
           Veriff : boolean;
        end;

   { Structure de stockage des resultats }
   classement     = record
                       nomEquipe       : string;
                       nbPoints        : integer;
                       nbButsMarques   : integer;
                       nbButsEncaisses : integer;
                       diff         : difference;
                    end;               
   tabclassement = array of classement;





(*--------------------------------------------------------------
 --nom            : changeEntier
 --rôle           : permet de changer une lettre en entier suivant l'alphabet
 --pré-conditions :  aucunes
 --résultat       : retourne un entier
 --auteur         :  Paul Brugat <brugatpaul@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION charToPos (lettre :char):integer;
VAR
   res : integer;
BEGIN
   If (lettre='-') then
   begin
      res:=0;
   end
   else
   begin
      res:= ord(lettre)-64;
   end;
   charToPos:=res;
END; { CharToPos }





(*--------------------------------------------------------------
 --nom            : sommeChiffres
 --rôle           : permet de calculer la somme des chiffres d'un nombre
 --pré-conditions :  aucunes
 --résultat       : retourne un entier
 --auteur         :  Paul Brugat <brugatpaul@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION sommeChiffres (nombre :integer):integer;
VAR
   quotient   : integer;
   reste      : integer;
   sommeReste : integer;
BEGIN
   sommeReste:=0;
   repeat
      quotient:=nombre div 10;
      reste:=nombre mod 10;
      sommeReste:=sommeReste+reste;
      nombre:=quotient;
   until (quotient=0);
   sommeChiffres:=sommeReste;
END; { sommeChiffres }





(*--------------------------------------------------------------
 --nom            : Nirvana
 --rôle           : permet d'appliquer la méthode du sage
 --pré-conditions :  aucunes
 --résultat       : fonction retourne un entier
 --auteur         :  Paul Brugat <brugatpaul@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION nirvana (prenom :string): integer;
VAR
   i	   : integer;
   somme   : integer;
BEGIN
   somme:=0;
   for i:=1 to length(prenom) do
   begin
      somme:=somme+charToPos(prenom[i]);
   end;
   somme:=somme*47+19;
   nirvana:=sommeChiffres(somme);   
END; { nirvana }





(*--------------------------------------------------------------
 --nom            : triInsertion
 --rôle           : permet de trier des entiers
 --pré-conditions :  aucunes
 --auteur         :  Paul Brugat <brugatpaul@eisti.eu>
 ----------------------------------------------------------------
 *)
procedure triInsertion(var t : tabdata);
var
   i,icourant : integer;
   x	      : data;
BEGIN
   for i:=0 to high(t) do
   begin
      icourant:=i;
      while (icourant<>0) and  (t[icourant].nb<t[icourant-1].nb) do
      begin
	 x:=t[icourant];
	 t[icourant]:=t[icourant-1];
	 t[icourant-1]:=x;
	 icourant:=icourant-1;
      end; 
   end;
END; { TriInsertion }





(*--------------------------------------------------------------
 --nom            : ReadData
 --rôle           : Permet la saisie des données et l'initialisation du tableau
 --pré-conditions : le nombre d'équipes (un entier)
 --résultat       : un tableau de classement de type tabclassement
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION ReadData (nbequipes :integer): tabclassement;
VAR
   i : integer;
BEGIN
   setlength(ReadData, nbequipes);
   for i:=0 to nbEquipes-1 do
   begin
      write('        Donner le nom de l''equipe ',i+1,' : ');
      readln(ReadData[i].nomEquipe);
      ReadData[i].nbButsMarques:=0;
      ReadData[i].nbButsEncaisses:=0;
      ReadData[i].nbPoints:=0;
   end;
END; { ReadData }





(*--------------------------------------------------------------
 --nom            : ResToPts
 --rôle           : À partir du tableau de résultat calcule le nombre de points d'une équipes
 --pré-conditions : un tableau de match de type tabmatch, un itérateur
 --résultat       : une structure de données de type classement
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION ResToPts (tab_match : tabmatch;i,j:integer ):integer;   
BEGIN
    if (tab_match[i].nbbut1>tab_match[i].nbbut2) then
         begin
            Case j of
              1 : ResToPts:=3;
              2 : ResToPts:=0;
            end;
         end
      else
         if (tab_match[i].nbbut1<tab_match[i].nbbut2) then
         begin
            Case j of
              1 : ResToPts:=0;
              2 : ResToPts:=3;
            end;
         end
      else
      begin
         ResToPts:=1;
      end;
END; { ResToPts }





(*--------------------------------------------------------------
 --nom            : CalculDiff
 --rôle           : Calcule la différence de but pour chaque équipe
 --pré-conditions : Le nombre de buts marqués,le nombre de buts encaissés et un itérateur (des entiers)
 --résultat       :  la différence de but une chaîne de caractères
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION CalculDiff (butsmarques,butsencaisses :integer ):difference; 
BEGIN
         If ((butsmarques-butsEncaisses)>=0)then
         begin
            CalculDiff.score:='+'+IntToStr(butsmarques-butsEncaisses);
            CalculDiff.points:=butsmarques-butsEncaisses;
         end
      else
         begin
            CalculDiff.score:=IntToStr(butsmarques-butsEncaisses);
            CalculDiff.points:=butsmarques-butsEncaisses;
         end;
END; { CalculDiff }





(*--------------------------------------------------------------
 --nom            : Ranking
 --rôle           : Permet à partir du tableau de résultats le calcul des points de chaque équipe
 --pré-conditions : un tableau de résultats de type tabmatch
 --résultat       :  un tableau de de type tabclassement
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION Ranking(nbequipes :integer):tabclassement;
var
   t_match : tabmatch;
   i,j,k   : integer;
Begin
   setlength(t_match,((nbequipes*(nbequipes-1))div 2));
   Ranking:=ReadData(nbequipes);
   k:=0;
   For i:=0 to nbequipes-1 do
   begin
      for j:=i+1 to nbequipes-1 do
      Begin
         write('Resultat du match ',Ranking[i].nomEquipe,'/',Ranking[j].nomEquipe,' : ');
         readln(t_match[k].nbBut1,t_match[k].nbBut2);
         Ranking[i].nbButsMarques:=Ranking[i].nbButsMarques+t_match[k].nbbut1;
         Ranking[i].nbButsEncaisses:=Ranking[i].nbButsEncaisses+t_match[k].nbbut2;
         Ranking[j].nbButsMarques:=Ranking[j].nbButsMarques+t_match[k].nbbut2;
         Ranking[j].nbButsEncaisses:=Ranking[j].nbButsEncaisses+t_match[k].nbbut1;
         Ranking[i].nbPoints:=Ranking[i].nbPoints+ResToPts(t_match,k,1);
         Ranking[j].nbPoints:=Ranking[j].nbPoints+ResToPts(t_match,k,2);
         k:=k+1;
      end;
      Ranking[i].diff:=CalculDiff(Ranking[i].nbButsMarques,Ranking[i].nbButsEncaisses);
   end;
END; { Ranking }





(*--------------------------------------------------------------
 --nom            : TriClassement
 --rôle           : Tri un tableau méthode par insertion
 --pré-conditions : un tableau de type tabclassement
 --résultat       :  Un tableau de type tabclassement trié
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION TriClassement (t : tabclassement): tabclassement;
VAR
   i,icourant : integer;
   x	      : classement;
BEGIN
   for i:=0 to length(t)-1 do
   begin
      icourant:=i;
      while (icourant<>0) and  (t[icourant].nbPoints>t[icourant-1].nbPoints) do
      begin
         x:=t[icourant];
         t[icourant]:=t[icourant-1];
         t[icourant-1]:=x;
         icourant:=icourant-1;
      end; 
   end;
   TriClassement:=t;
END; { TriClassement }




(*--------------------------------------------------------------
 --nom            : TriClassement2
 --rôle           : Tri un tableau méthode par insertion en fonction de la différence de buts
 --pré-conditions : un tableau de type tabclassement, un indice de début et de fin
 --résultat       :  Un tableau de type tabclassement trié
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION TriClassement2 (t : tabclassement;ideb,ifin:integer): tabclassement;
VAR
   i,icourant : integer;
   x	      : classement;
BEGIN
   for i:=ideb to ifin-1 do
   begin
      icourant:=i;
      while (icourant<>0) and  (t[icourant].diff.points>t[icourant-1].diff.points) do
      begin
         x:=t[icourant];
         t[icourant]:=t[icourant-1];
         t[icourant-1]:=x;
         icourant:=icourant-1;
      end; 
   end;
   TriClassement2 :=t;
END; { TriClassement2 }




(*--------------------------------------------------------------
 --nom            : TriClassement3
 --rôle           : Tri un tableau méthode par insertion en fonction des buts marqués
 --pré-conditions : un tableau de type tabclassement, un indice de début et de fin
 --résultat       :  Un tableau de type tabclassement trié
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION TriClassement3 (t : tabclassement;ideb,ifin:integer): tabclassement;
VAR
   i,icourant : integer;
   x	      : classement;
BEGIN
   for i:=ideb to ifin-1 do
   begin
      icourant:=i;
      while (icourant<>0) and  (t[icourant].nbbutsmarques>t[icourant-1].nbbutsmarques) do
      begin
         x:=t[icourant];
         t[icourant]:=t[icourant-1];
         t[icourant-1]:=x;
         icourant:=icourant-1;
      end; 
   end;
   TriClassement3:=t;
END; { TriClassement3 }




(*--------------------------------------------------------------
 --nom            : TriClassement4
 --rôle           : Tri un tableau méthode par insertion en fonction de l'ordre alphabétique
 --pré-conditions : un tableau de type tabclassement, un indice de début et de fin 
 --résultat       :  Un tableau de type tabclassement trié
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION TriClassement4(t : tabclassement;ideb,ifin:integer): tabclassement;
VAR
   i,j,k: integer;
   x	      : classement;
BEGIN
   for i:=ideb to ifin do
      Begin
         k:=1;
         J:=I;
         If(t[j].nomequipe[k]<>t[j-1].nomequipe[k])then
            Begin
               While(Chartopos(t[j].nomequipe[k])<Chartopos(t[j-1].nomequipe[k]))do
               Begin
                  x:=t[j];
                  t[j]:=t[j-1];
                  t[j-1]:=x;
                  j:=j-1;
               end;      
            End
         else
            If(t[j].nomequipe[k]=t[j-1].nomequipe[k])then
               Begin
                  While (t[j].nomequipe[k]=t[j-1].nomequipe[k])do
                     Begin
                        k:=k+1;
                     end;
                  While(Chartopos(t[j].nomequipe[k])<Chartopos(t[j-1].nomequipe[k]))do
                  Begin
                     x:=t[j];
                     t[j]:=t[j-1];
                     t[j-1]:=x;
                     j:=j-1;
                  end;      
               end;
      end;
   TriClassement4:=t;
END; { TriClassement4 }





(*--------------------------------------------------------------
 --nom            : VerifEgalite
 --rôle           : Vérifie si une égalité est présente dans un classement
 --pré-conditions : un tableau de classement de type tabclassement déjà trié
 --résultat       : un bouléen
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION VerifEgalite (t_classement :tabclassement ):boolean;
VAR
   i : integer;
BEGIN
   VerifEgalite:=false;
   For i :=0 to length(t_classement)-1 do
      Begin
         if (t_classement[i].nbpoints=t_classement[i+1].nbpoints)then
            begin
               VerifEgalite:=True;
            end;
      end;
END; { VerifEgalite }





(*--------------------------------------------------------------
 --nom            : Egalitepts
 --rôle           : Permet d'appliquer les règles de l'égalité
 --pré-conditions : un tableau de type tabclassement
 --résultat       :  un tableau classé selon les règles de l'égalité
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION Egalitepts (tab:tabclassement ):tabclassement;
VAR
   i,j : integer;
BEGIN;
   for i:=0 to length(tab)-1 do
      Begin
         j:=i+1;
         while((tab[i].nbpoints=tab[j].nbpoints)) do
         Begin
               j:=j+1;
            End;
         Egalitepts:=TriClassement2(tab,i,j-1);
      end;
END;






(*--------------------------------------------------------------
 --nom            :  EgaliteDiff
 --rôle           :  Applique les règles de l'égalité en cas d'égalité de différence
 --pré-conditions :  un tableau de classement de type tabclassement
 --résultat       :  un tableau de type tabclassement trié selon les règles de l'égalité
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION  Egalitediff (tab :tabclassement ):tabclassement;
Var
   i,k : integer;
Begin
   for i:=0 to length(tab)-1 do
   Begin
      k:=i+1;
      while((tab[i].nbpoints=tab[k].nbpoints)and (tab[i].diff.points=tab[k].diff.points))do
      Begin
         k:=k+1;
      End;
      Egalitediff:=TriClassement3(tab,i,k-1);
   end;
End; { Egalitediff }





(*--------------------------------------------------------------
 --nom            :  Egalitebuts
 --rôle           :  Applique les règles de l'égalité en cas d'égalité de buts
 --pré-conditions :  un tableau de classement de type tabclassement
 --résultat       :  Un tableau de classement de type tabclassement
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION  Egalitebuts (tab :tabclassement):tabclassement;
VAR
   i,l     : integer;
Begin
   i:=0;
   while i<>length(tab)do
   begin
      l:=i+1;
      While((tab[i].nbpoints=tab[l].nbpoints)and(tab[i].diff.points=tab[l].diff.points)and(tab[i].nbbutsmarques=tab[l].nbbutsmarques))do
      Begin
         l:=l+1;
      End;
      Egalitebuts:=TriClassement4(tab,i,l-1);
      i:=l;
   End;
END; {Egalitebuts}





(*--------------------------------------------------------------
 --nom            : DispClassement
 --rôle           : Tri et affiche le classement
 --pré-conditions : un tableau de classement de type tabclassement
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
PROCEDURE DispClassement (tab_classement :tabclassement );
Var
   i   : integer;
   tab : tabclassement;
BEGIN
   tab:=triClassement(tab_classement);
   If (VerifEgalite(tab))then
      Begin
         tab:=Egalitebuts(Egalitediff(Egalitepts(tab)));
      End;
   writeln('           ----------');
   writeln('           CLASSEMENT');
   writeln('           ----------');
   writeln('         EQUIPE       POINTS  BUTS   DIFF');
   for i:=0 to high(tab) do
   begin
      writeln(i+1:3,'.',tab[i].nomEquipe:15,' ',tab[i].nbPoints:6,'    ',tab[i].nbButsMarques,':',tab[i].nbButsEncaisses,'   ',tab[i].diff.score:3);
   end;
END; { DispClassement }





(*--------------------------------------------------------------
 --nom            : Menu1
 --rôle           : Permet la coordination des actions pour calculer le nombre de réincarnation selon la méthode du grand sage
 --pré-conditions :  aucunes
 --auteur         :  Maxime Lescaud <lescaudmax@eisti.eu>
 ----------------------------------------------------------------
 *)
PROCEDURE Menu1 ();
VAR
   i	    : integer;
   tab	    : tabdata;
   nbprenom : integer;
BEGIN
   Clrscr;
   write('Nombre de prénoms à rentrer : ');
   readln(nbprenom);
   setlength(tab,nbprenom);
   for i:=0 to high(tab) do
   begin
      write('Tapez le prénom ',i+1,' : ');
      readln(tab[i].name);
      tab[i].name:=uppercase(tab[i].name);
      tab[i].nb:=nirvana(tab[i].name);
   end;
   triInsertion(tab);
   for i:=0 to high(tab) do
      writeln(tab[i].name,' : ', tab[i].nb);
end; { Menu1 }





(*--------------------------------------------------------------
 --nom            : Menu2
 --rôle           : Permet la coordination des actions pour le grand tournoi de Trifouillis-les-Oies
 --pré-conditions :  aucunes
 --auteur         :  Maxime Lescaud <lescaudmax@eisti.eu>
 ----------------------------------------------------------------
 *)
PROCEDURE Menu2 ();
var
   nbEquipes	  : integer;
BEGIN
   Clrscr;
   { Saisie des equipes et des resultats }
   writeln('   ============================');
   writeln('   GRAND TOURNOI DE FOOT DE Trifouillis-les-Oies');
   writeln('   ============================');
   write('Donner le nombre d''equipes engagees : ');
   readln(nbEquipes);
   DispClassement(Ranking(nbequipes));
end; { Menu2 }





var
   choix:  integer;
BEGIN
repeat
      Begin
         writeln('*********************************************');
         writeln('*********************************************');
         writeln('           MENU MINI PROJET TRI');
         writeln('*********************************************');
         writeln('*********************************************');
         writeln('Que voulez-vous faire?');
         writeln('1. Réincarnation ');
         writeln('2. Tournoi de football');
         writeln('0. Quitter');
         readln(choix);
         case choix of
           1 : Menu1();
           2 : Menu2();
         end; { case }
      End;
Until (choix=0);
END.
